<?php
/**
 * @author      Matthias Büsing <info@mb-tec.eu>
 * @copyright   Copyright (c) 2018 Matthias Büsing
 * @link        https://mb-tec.eu
 * @license     MIT
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'MbTec_TermsAndConditions',
    __DIR__
);