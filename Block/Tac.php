<?php
/**
 * @author      Matthias Büsing <info@mb-tec.eu>
 * @copyright   Copyright (c) 2018 Matthias Büsing
 * @link        https://mb-tec.eu
 * @license     MIT
 */

namespace MbTec\TermsAndConditions\Block;

use Magento\Framework\View\Element\Template;
use Magento\CheckoutAgreements\Model\CheckoutAgreementsRepository;

/**
 * Class Tac
 * @package MbTec\TermsAndConditions\Block
 */
class Tac extends Template
{
    /**
     * @var CheckoutAgreementsRepository
     */
    private $agreementsRepo;

    /**
     * Tac constructor.
     * @param CheckoutAgreementsRepository $agreementsRepo
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        CheckoutAgreementsRepository $agreementsRepo,
        Template\Context $context,
        array $data = []
    ) {
        $this->agreementsRepo = $agreementsRepo;

        parent::__construct($context, $data);
    }

    /**
     * @return \Magento\CheckoutAgreements\Api\Data\AgreementInterface[]
     */
    public function getAgreementsCollection()
    {
        return $this->agreementsRepo->getList();
    }
}
